package com.bdd.app;

import com.bdd.app.pages.ConfigurePage;
import com.bdd.app.pages.LoginPage;
import com.bdd.app.pages.ReportingPage;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class StepDefs {

    WebDriver driver;
    String url;
    LoginPage loginPage;
    ConfigurePage configurePage;
    ReportingPage reportingPage;

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        url = "http://simplerisk.local/index.php";
        loginPage = new LoginPage(driver);
        configurePage = new ConfigurePage(driver);
        reportingPage = new ReportingPage(driver);
        driver.get(url);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {

        try {
            reportingPage.logout();
        } catch (Exception e) {
            System.out.println(e);
        }

        driver.quit();
    }


    @Given("^a user called (.+) with (.+) permissions and password (.+)$")
    public void a_user_exists(String username, String permissions, String password) throws Throwable {
        loginPage.enterUsername("admin");
        loginPage.enterPassword("^Y&U8i9o0p");
        loginPage.login();
        reportingPage.clickConfigureLink();
        configurePage.clickUserManagementLink();
        if(configurePage.checkIfUserIsPresent(username)){configurePage.logout();}
        else{
            configurePage.enterNewUsername(username);
            configurePage.enterNewPassword(password);
            configurePage.givePermissions(permissions);
            configurePage.createUser();
            configurePage.logout();
        }
    }


    @When("^(.+) is logged in with password (.+)$")
    public void user_is_logged_in(String username, String password) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^he is able to view (.+)'s account$")
    public void able_to_view_users_account(String username) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
