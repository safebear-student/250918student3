package com.bdd.app.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by CCA_Student on 06/12/2017.
 */
public class ConfigurePage extends BasePage {

    WebDriver driver;
    @FindBy(partialLinkText = "User Management")
    WebElement user_management_link;

    @FindBy(id = "user")
    WebElement user_details_dropdown;

    @FindBy(name = "username")
    WebElement username_details;

    @FindBy(name = "new_user")
    WebElement new_username;

    @FindBy(name = "password")
    WebElement new_password;

    @FindBy(name = "repeat_password")
    WebElement repeat_password;

    @FindBy(name = "check_all")
    WebElement checkall_checkbox;

    @FindBy(name = "check_risk_mgmt")
    WebElement check_risk_mgmt_checkbox;

    @FindBy(xpath = "//input[@name=\"add_user\"]")
    WebElement add_user_button;

    @FindBy(name = "name")
    WebElement name_field;



    public ConfigurePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);

    }

    public void clickUserManagementLink(){
        user_management_link.click();
    }

    public void viewUserDetails(String user){
        Actions action = new Actions(driver);
        action.moveToElement(user_details_dropdown);
        action.perform();
        Select select_user = new Select(user_details_dropdown);
        select_user.selectByVisibleText(user);
        user_details_dropdown.submit();
    }

    public Boolean checkUserDetails(String user){
        return username_details.getAttribute("value").contains(user);
    }

    public boolean checkIfUserIsPresent(String user){
        Actions action = new Actions(driver);
        action.moveToElement(user_details_dropdown);
        action.perform();
        Select select_user = new Select(user_details_dropdown);
        List<WebElement> options = select_user.getOptions();

        for (WebElement option : options){

            if (option.getText().contains(user)){
                return true;
            }
        }
        return false;

    }

    public void enterNewUsername(String username){
        new_username.sendKeys(username);
        name_field.sendKeys(username);
    }

    public void enterNewPassword(String password){
        new_password.sendKeys(password);
        repeat_password.sendKeys(password);
    }

    public void givePermissions(String permissions){

        Actions action = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor)driver;

        switch (permissions){
            case "administrator":
                action.moveToElement(checkall_checkbox);
                action.perform();
                // action.click(checkall_checkbox);
                // action.perform();
                js.executeScript("arguments[0].click();", checkall_checkbox);

                //checkall_checkbox.click();
                break;
            case "risk_management":
                action.moveToElement(check_risk_mgmt_checkbox);
                action.perform();
                //action.click(check_risk_mgmt_checkbox);
                //  action.perform();

                js.executeScript("arguments[0].click();", check_risk_mgmt_checkbox);
               // check_risk_mgmt_checkbox.click();
                break;
            default:
                action.moveToElement(check_risk_mgmt_checkbox);
                action.perform();
                js.executeScript("arguments[0].click();", check_risk_mgmt_checkbox);
                //check_risk_mgmt_checkbox.click();
                break;
        }
    }

    public void createUser(){
        Actions action = new Actions(driver);
        action.moveToElement(add_user_button);
        action.perform();
        add_user_button.click();
    }


}
