Feature: User Management

  User Story:
  In order to manage users
  As an admin
  I need CRUD on user accounts

  Rules:
  - Non admin users have no permissions over other users
  - There are 2 types of admin
  -- Risk
  -- Asset managers

  Questions:
  - Do users need access to all accounts or just their group

  To Do:
  - Force reset of password on account creation

  Domain Language:
  CRUD = Create, Read, Update, Delete
  Group = Users are identified by the group they belong in

  Background:
    Given a user called simon with administrative permissions and password S@feB3ar
    And a user called tom with Risk_management permissions and password S@feB3ar


  @high-risk
  Scenario Outline: The admin checks a users details
    When simon is logged in with password S@feB3ar
    Then he is able to view <user>'s account
    Examples:
      | user |
      | Tom  |

  @High-impact
  @to-do
  Scenario Outline: A user's password is reset by admin
    Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then simon can reset <user>'s password
    Examples:
      | user |
      | Tom  |
